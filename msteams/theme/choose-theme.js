import { ThemeStyle } from 'msteams-ui-components-react';

export const chooseTheme = (themeStr) => {
  let chosenTheme;
  switch (themeStr) {
    case 'dark':
      chosenTheme = ThemeStyle.Dark;
      break;
    case 'contrast':
      chosenTheme = ThemeStyle.HighContrast;
      break;
    case 'default':
    default:
      chosenTheme = ThemeStyle.Light;
  }
  return chosenTheme;
};

export default chooseTheme;

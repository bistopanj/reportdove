import { BotFrameworkAdapter } from 'botbuilder';

const isProduction = process.env.NODE_ENV === 'production';

const appId = isProduction
  ? process.env.MICROSOFT_APP_ID
  : process.env.MICROSOFT_APP_ID_DEV;
const appPassword = isProduction
  ? process.env.MICROSOFT_APP_SECRET
  : process.env.MICROSOFT_APP_SECRET_DEV;

export const buildAdapter = () => {
  const theAdapter = new BotFrameworkAdapter({ appId, appPassword });
  theAdapter.onTurnError = async (context, error) => {
    // This check writes out errors to console log .vs. app insights.
    // NOTE: In production environment, you should consider logging this to Azure
    //       application insights.
    console.error(`\n [onTurnError] unhandled error: ${error}`);

    // Send a trace activity, which will be displayed in Bot Framework Emulator
    await context.sendTraceActivity(
      'OnTurnError Trace',
      `${error}`,
      'https://www.botframework.com/schemas/error',
      'TurnError'
    );

    // Send a message to the user
    await context.sendActivity('The bot encountered an error or bug.');
    await context.sendActivity(
      'To continue to run this bot, please fix the bot source code.'
    );
  };

  return theAdapter;
};

const adapter = buildAdapter();

export default adapter;

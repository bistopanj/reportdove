import { TeamsActivityHandler } from 'botbuilder';
import findTask from './../../brain/tasks/find-task';
import respondMessage from '../../brain/responder/respond-message';
import processAction from './../../brain/actions/process-action';
import {
  respondFileConsentDecline,
  uploadAndSendFile,
} from '../../brain/responder/send-file';
import handleMemberAdded from '../../brain/event-manager/member-added';
import handleMemberRemoved from '../../brain/event-manager/member-removed';

class MessageExtensionBot extends TeamsActivityHandler {
  constructor() {
    super();

    this.onTurn(async (context, next) => {
      console.log(`onTurn`);
      // await context.sendActivity('pong');
      await next();
    });
    // this.onConversationUpdate(saveConversationReference);
    this.onMembersAdded(handleMemberAdded);
    this.onMembersRemoved(handleMemberRemoved);
    this.onMessage(respondMessage);
  }

  async handleTeamsTaskModuleSubmit(context, action) {
    console.log('handleTeamsTaskModuleSubmit');
    const response = await processAction(context, action);
    return response;
  }

  // Messaging Extensions

  handleTeamsMessagingExtensionFetchTask(context, action) {
    console.log('handleTeamsMessagingExtensionFetchTask');
    const task = findTask(context, action);
    return task;
  }

  async handleTeamsMessagingExtensionSubmitAction(context, action) {
    console.log('handleTeamsMessagingExtensionSubmitAction');
    const response = await processAction(context, action);
    return response;
  }

  // Sending Files
  async handleTeamsFileConsentAccept(context, fileConsentCardResponse) {
    console.log('handleTeamsFileConsentAccept');
    await uploadAndSendFile(context, fileConsentCardResponse);
  }

  async handleTeamsFileConsentDecline(context, fileConsentCardResponse) {
    console.log('handleTeamsFileConsentDecline');
    await respondFileConsentDecline(context, fileConsentCardResponse);
  }
}

export default MessageExtensionBot;

const createFileAttachment = ({
  uploadResult,
  fileType = 'pdf',
  fileName = 'fileName.pdf',
}) => {
  const guid = uploadResult.eTag.match(/\{(.*?)\}/)[1];
  const downloadCard = {
    uniqueId: guid,
    fileType,
  };
  const attachment = {
    content: downloadCard,
    contentType: 'application/vnd.microsoft.teams.card.file.info',
    name: fileName,
    contentUrl: uploadResult.webUrl,
  };
  return attachment;
};

export default createFileAttachment;

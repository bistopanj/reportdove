const sendCard = async (context, card) => {
  try {
    await context.sendActivity({ attachments: [card] });
  } catch (error) {
    console.log('error in sending card:');
    console.log(error);
  }
};

export default sendCard;

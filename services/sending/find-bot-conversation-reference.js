import findConversationRefForUser from '../db/conversation-ref/find-conversation-ref-for-user';

const findBotConversationReference = async (context) => {
  const conversationRef = await findConversationRefForUser(
    context.activity.from
  );
  return conversationRef;
};

export const findBotConversationReferenceWithUser = async (aadObjectId) =>
  findBotConversationReference({ activity: { from: { aadObjectId } } });

export default findBotConversationReference;

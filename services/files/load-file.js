import redisClient from './../../dbs/cache/redis';

export const loadFile = async (fileKey) => {
  const fileStr = await redisClient.getAsync(`FILE:${fileKey}`);
  const fileBuffer = new Buffer(fileStr, 'hex');
  return fileBuffer;
};

export default loadFile;

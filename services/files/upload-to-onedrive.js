import { Client, LargeFileUploadTask } from '@microsoft/microsoft-graph-client';

const uploadToOnedrive = async ({
  fileBuffer,
  fileName = 'NewFile',
  folderName = 'my-reports',
  accessToken,
}) => {
  const msGraphClient = createMsGraphClient(accessToken);
  const requestUrl = `/me/drive/special/approot:/${folderName}/${fileName}:/createUploadSession`;
  // const requestUrl = `/me/drive/root:/${folderName}/${fileName}:/createUploadSession`;
  const sessionPayload = {
    item: {
      '@microsoft.graph.conflictBehavior': 'fail',
      name: fileName,
    },
  };
  const fileObject = {
    size: fileBuffer.byteLength,
    content: fileBuffer,
    name: fileName,
  };

  try {
    const uploadSession = await LargeFileUploadTask.createUploadSession(
      msGraphClient,
      requestUrl,
      sessionPayload
    );
    const uploadTask = new LargeFileUploadTask(
      msGraphClient,
      fileObject,
      uploadSession
    );

    const uploadResult = await uploadTask.upload();

    return uploadResult;
  } catch (error) {
    console.log('error in uploadToOnedrive');
    console.log(error);
    throw error;
  }
};

function createMsGraphClient(token) {
  const authProvider = (cb) => {
    cb(null, token);
  };
  const options = { authProvider };
  const msGraphClient = Client.init(options);
  return msGraphClient;
}

export default uploadToOnedrive;

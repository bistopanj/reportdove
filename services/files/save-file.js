import { v4 as uuidv4 } from 'uuid';
import redisClient from './../../dbs/cache/redis';

const EXPIRATION_MINS = 60 * 24;

export const saveFile = async (fileBuffer, key) => {
  console.log('saving file to redis');
  let fileKey = key;
  if (!fileKey) fileKey = uuidv4();

  await redisClient.setexAsync(
    `FILE:${fileKey}`,
    EXPIRATION_MINS * 60,
    fileBuffer.toString('hex')
  );
  console.log(`file saved to redis with key ${fileKey}`);
  return fileKey;
};

export default saveFile;

export const createReportFileName = () =>
  `report-${new Date().toISOString().substring(0, 19).replace(/:/g, '')}.pdf`;

export default createReportFileName;

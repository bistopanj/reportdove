import PDFDocument from 'pdfkit';
import loadReportImages from './load-report-images';

export const createPdfReport = async (reportData) => {
  return new Promise(async (resolve, reject) => {
    const doc = new PDFDocument();
    const imageWidth =
      doc.page.width - doc.page.margins.right - doc.page.margins.left;
    const buffers = [];
    doc.on('data', buffers.push.bind(buffers));
    doc.on('end', () => {
      const pdfBuffer = Buffer.concat(buffers);
      resolve(pdfBuffer);
    });
    doc.text(reportData.generalReport);
    const images = await loadReportImages(reportData);
    reportData.files.forEach((file) => {
      doc.addPage();
      doc.image(images[file.key], {
        fit: [imageWidth, imageWidth],
      });
      doc.moveDown();
      doc.text(file.description);
    });
    doc.end();
  });
};

export default createPdfReport;

import loadFile from '../files/load-file';

export const loadReportImages = async (reportData) => {
  const imageMap = {};
  const images = reportData.files.map((file) => file.key);
  const imagePromises = images.map((key) => loadFile(key));
  const buffers = await Promise.all(imagePromises);
  images.forEach((key, index) => {
    imageMap[key] = buffers[index];
  });
  return imageMap;
};

export default loadReportImages;

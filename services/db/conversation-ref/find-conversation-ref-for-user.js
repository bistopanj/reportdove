import connectToDatabase from '../../../dbs/permanent/mongo';

const findConversationRefForUser = async (user) => {
  const db = await connectToDatabase();
  const { aadObjectId } = user;
  const conversationRef = await db
    .collection('conversation-references')
    .findOne({
      'user.aadObjectId': aadObjectId,
      'conversation.conversationType': 'personal',
    });
  return conversationRef;
};

export default findConversationRefForUser;

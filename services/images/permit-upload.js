const permitUpload = () => {
  const timestamp = ((Date.now() / 1000) | 0).toString();
  const params = {
    timestamp,
  };
  const uploadUrl = `/api/images/upload`;
  return { uploadUrl, formDataInfo: params };
};

export default permitUpload;

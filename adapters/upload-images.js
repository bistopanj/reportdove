const doNothing = () => ({});

const getUploadPermission = async () => {
  try {
    const res = await fetch(`/api/images/permissions`);
    const { uploadUrl, formDataInfo } = await res.json();
    return { uploadUrl, formDataInfo };
  } catch (error) {
    console.log(error);
  }
};

const buildFormData = (info, file) => {
  const formData = new FormData();
  Object.keys(info).forEach((key) => {
    formData.append(key, info[key]);
  });
  formData.append('file', file);
  return formData;
};

const xhr = (url, options) => {
  const request = new XMLHttpRequest();
  const method = options.method || 'get';
  const onSuccess = options.onSuccess || doNothing;
  const onError = options.onError || doNothing;
  request.open(method, url, true);
  request.responseType = options.responseType || 'json';
  if (options.onProgress) request.onprogress = options.onProgress;
  if (options.onUploadProgress)
    request.upload.onprogress = options.onUploadProgress;
  if (options.onStart) request.onloadstart = options.onStart;
  request.onload = () => {
    if (request.status >= 200 && request.status < 400) {
      return onSuccess(request.response);
    }

    return onError(request);
  };

  request.onabort = () => {
    onError(request);
  };

  request.onerror = () => {
    onError(request);
  };
  request.ontimeout = () => {
    onError(request);
  };

  request.send(options.body || null);
};

const uploadImage = async (
  file,
  onSuccess = doNothing,
  onError = doNothing,
  onProgress = doNothing
) => {
  const { uploadUrl, formDataInfo } = await getUploadPermission();
  const formData = buildFormData(formDataInfo, file.file);
  const options = {
    body: formData,
    method: 'Post',
    onUploadProgress: onProgress,
    onSuccess,
    onError,
  };
  xhr(uploadUrl, options);
};

const uploadImages = (
  files = [],
  reportFinish = doNothing,
  reportError = doNothing,
  reportProgress = doNothing
) => {
  files.forEach((file) =>
    uploadImage(
      file,
      reportFinish.bind(null, file.id),
      reportError.bind(null, file.id),
      reportProgress.bind(null, file.id)
    )
  );
};

export default uploadImages;

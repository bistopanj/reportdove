const checkReportAvailability = async (token) => {
  if (!token) return false;

  const response = await fetch(`/api/reports?token=${token}&check=true`);
  return response.status === 200;
};

export default checkReportAvailability;

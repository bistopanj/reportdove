import { v4 as uuidv4 } from 'uuid';
import loadConfig from '../utils/load-config';
import buildQueryString from '../utils/build-query-string';

const isProduction = process.env.NODE_ENV === 'production';

const botId = isProduction
  ? process.env.MICROSOFT_APP_ID
  : process.env.MICROSOFT_APP_ID_DEV;

const config = loadConfig();

const authTaskInfo = {
  width: 'medium',
  height: 'medium',
  completionBotId: botId,
  title: 'Sending Your Report Data',
  url: `${config.BASE_URL}/tasks/auth`,
  fallbackUrl: `${config.BASE_URL}/tasks/auth`,
};

const submitAction = ({ job, payload, accessToken }) =>
  new Promise((resolve, reject) => {
    const reference = uuidv4();
    const submitHandler = (err, result) => {
      resolve(reference);
    };
    const queryParams = {
      reference,
      job,
      payload: JSON.stringify(payload),
    };
    if (accessToken) queryParams.accessToken = accessToken;
    const queryString = buildQueryString(queryParams);
    const baseUrl = `${config.BASE_URL}/tasks/auth`;
    const authUrl = `${baseUrl}?${queryString}`;
    authTaskInfo.url = authUrl;
    authTaskInfo.fallbackUrl = authUrl;
    microsoftTeams.tasks.startTask(authTaskInfo, submitHandler);
  });

export default submitAction;

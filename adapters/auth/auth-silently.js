import { UserAgentApplication } from 'msal';
import msalConfig from './msal-config';
import scopes from './scopes';

const authSilently = async (context = {}) => {
  try {
    const { tid, loginHint } = context;
    const authority = `https://login.microsoftonline.com/${tid}`;
    const msalConfigUpdated = {
      ...msalConfig,
      auth: { ...msalConfig.auth, authority },
    };
    const userAgentApplication = new UserAgentApplication(msalConfigUpdated);
    const tokenRequest = { scopes, loginHint };
    const authInfo = await userAgentApplication.acquireTokenSilent(
      tokenRequest
    );
    return authInfo;
  } catch (error) {
    console.log('no auth info');
    return null;
  }
};

export default authSilently;

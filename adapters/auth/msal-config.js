import loadConfig from '../../utils/load-config';

const config = loadConfig();

const msalConfig = {
  auth: {
    clientId: config.BOT_ID,
    authority: 'https://login.microsoftonline.com/common',
    // postLogoutRedirectUri: `${config.BASE_URL}/auth/logout-end`,
    // redirectUri: `${config.BASE_URL}/auth/end`,
  },
  cache: {
    cacheLocation: 'localStorage',
    storeAuthStateInCookie: false,
  },
};

export default msalConfig;

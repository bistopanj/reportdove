const login = (context) =>
  new Promise((resolve, reject) => {
    if (!microsoftTeams) reject(new Error('microsoft teams not available'));
    const { tid, loginHint } = context || {};
    const url = `${window.location.origin}/auth/start?tid=${tid}&loginHint=${loginHint}`;
    microsoftTeams.authentication.authenticate({
      url,
      width: 600,
      height: 600,
      successCallback: (result) => {
        console.log('success callback');
        resolve(result);
      },
      failureCallback: (reason) => {
        console.log(reason);
        reject(new Error(`Failed to sign in. Reason: ${reason}`));
      },
    });
  });

export default login;

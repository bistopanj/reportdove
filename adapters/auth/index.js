import { UserAgentApplication } from 'msal';
import getMicrosoftContext from '../../utils/msteams/get-microsoft-context';
import twoStepSignin from './two-step-signin';
import msalConfig from './msal-config';
import scopes from './scopes';

// export const userAgent =
//   window && window.navigator ? new UserAgentApplication(msalConfig) : null;

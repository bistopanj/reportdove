import scopes from './scopes';

const getToken = (userAgent, context) =>
  new Promise(async (resolve, reject) => {
    if (!microsoftTeams) reject(new Error('microsoft teams not available'));

    try {
      const { loginHint } = context || {};
      const tokenRequest = { scopes, loginHint };
      const response = await userAgent.acquireTokenSilent(tokenRequest);
      resolve(response.accessToken);
    } catch (err) {
      if (err.errorMessage.indexOf('interaction_required') !== -1) {
        // alert('should get token interactively');
        const { tid, loginHint } = context || {};
        const url = `${window.location.origin}/auth/start?tid=${tid}&loginHint=${loginHint}&accessToken=true`;
        microsoftTeams.authentication.authenticate({
          url,
          width: 600,
          height: 600,
          successCallback: (result) => {
            console.log('success callback');
            // alert('token acquired successfully');
            resolve(result);
          },
          failureCallback: (reason) => {
            // alert(`Token acquisition failed: ${reason}`);
            console.log(reason);
            reject(new Error(`Failed to get access token. Reason: ${reason}`));
          },
        });
      } else {
        reject(error);
      }
    }
  });

export default getToken;

import scopes from './scopes';

const getCachedToken = async (userAgent, context) => {
  try {
    const { loginHint } = context;
    const request = { scopes, loginHint };
    const result = await userAgent.acquireTokenSilent(request);
    return result.accessToken;
  } catch (error) {
    return null;
  }
};

export default getCachedToken;

const scopes = [
  'https://graph.microsoft.com/User.Read',
  // 'https://graph.microsoft.com/Files.ReadWrite.AppFolder',
  'https://graph.microsoft.com/Files.ReadWrite',
];

export default scopes;

import Promise from 'bluebird';

const redisURI = process.env.REDIS_URI;
const redis = Promise.promisifyAll(require('redis'));

const redisClient = redis.createClient(redisURI);

redisClient.on('connect', () => {
  console.log('successfully connected to redis');
});

redisClient.on('ready', () => {
  console.log('redis connection is ready.');
});

redisClient.on('reconnecting', () => {
  console.log('reconnecting to redis...');
});
redisClient.on('error', (error) => {
  console.log('error connecting to redis client: ');
  console.log(error);
});

redisClient.on('end', () => {
  console.log('closed redis client');
});

export const closeRedisInstance = () => redisClient.quitAsync();

export default redisClient;

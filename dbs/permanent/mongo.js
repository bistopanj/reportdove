import { MongoClient } from 'mongodb';
import assert from 'assert';

const dbURI = process.env.MONGODB_URI;
const dbName = process.env.MONGODB_DB_NAME;

const options = {
  poolSize: 50,
  useUnifiedTopology: true,
  native_parser: true,
};

let db;

const client = new MongoClient(dbURI, options);

const connectToDatabase = async () => {
  console.log(`isConnected: ${client.isConnected()}`);
  if (db) {
    console.log('An active connection to database exists');
    return db;
  }

  try {
    await client.connect();
    console.log('new connection to mongodb created');
    db = client.db(dbName);
    return db;
  } catch (error) {
    console.log('error in connecting to mongodb');
    console.log(error);
  }
};

export const closeDbConnection = () => {
  client.close();
};

export default connectToDatabase;

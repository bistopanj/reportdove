import isInTeams from './is-in-teams';

const getMicrosoftContext = () =>
  new Promise((resolve, reject) => {
    // if (!isInTeams()) return resolve(null);
    if (!microsoftTeams) {
      return resolve(null);
    }
    microsoftTeams.initialize();
    microsoftTeams.getContext((context) => {
      resolve(context);
    });
  });

export default getMicrosoftContext;

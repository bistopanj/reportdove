export const initializeTeams = () => {
  if (typeof window !== 'undefined' && window.microsoftTeams) {
    console.log('initializing microsoftTeams');
    window.microsoftTeams.initialize();
  }
};

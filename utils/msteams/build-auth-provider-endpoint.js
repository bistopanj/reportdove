import { v4 as uuidv4 } from 'uuid';
import loadConfig from './../load-config';
import buildQueryString from './../build-query-string';

const config = loadConfig();

export const buildAuthProviderEndpoint = (context, state) => {
  let baseUrl = config.BASE_URL;

  try {
    baseUrl = window.location.origin;
  } catch (error) {
    console.log(error);
  }

  let queryParams = {
    client_id: config.BOT_ID,
    response_type: 'id_token token',
    response_mode: 'fragment',
    scope: 'https://graph.microsoft.com/User.Read openid',
    redirect_uri: `${window.location.origin}/auth/end`,
    nonce: uuidv4(),
    state: state,
    login_hint: context.loginHint,
  };

  const authorizeEndpoint = `https://login.microsoftonline.com/${
    context.tid
  }/oauth2/v2.0/authorize?${buildQueryString(queryParams)}`;

  return authorizeEndpoint;
};

export default buildAuthProviderEndpoint;

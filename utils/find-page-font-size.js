export const findPageFontSize = () => {
  let sizeStr = window
    .getComputedStyle(document.getElementsByTagName('html')[0])
    .getPropertyValue('font-size');
  sizeStr = sizeStr.replace('px', '');
  let fontSize = parseInt(sizeStr, 10);
  if (!fontSize) {
    fontSize = 16;
  }
  return fontSize;
};

export default findPageFontSize;

import config from '../config';

const loadConfig = () => {
  return process.env.NODE_ENV in config
    ? config[process.env.NODE_ENV]
    : config.development;
};

export default loadConfig;

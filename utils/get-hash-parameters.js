const getHashParameters = (hashString) => {
  const hashParams = {};
  hashString
    .substr(1)
    .split('&')
    .forEach((item) => {
      const s = item.split('='),
        k = s[0],
        v = s[1] && decodeURIComponent(s[1]);
      hashParams[k] = v;
    });
  return hashParams;
};

export default getHashParameters;

import FileType from 'file-type';

const buildBase64DataUri = async (buffer) => {
  const data = buffer.toString('base64');
  const fileType = await FileType.fromBuffer(buffer);
  return `data:${fileType.mime};base64,${data}`;
};

export default buildBase64DataUri;

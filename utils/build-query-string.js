const buildQueryString = (params) => {
  let encodedQueryParams = [];
  for (let key in params) {
    encodedQueryParams.push(key + '=' + encodeURIComponent(params[key]));
  }
  return encodedQueryParams.join('&');
};

export default buildQueryString;

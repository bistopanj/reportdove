import { useEffect, useState, useCallback } from 'react';
import Head from 'next/head';
import {
  TeamsThemeContext,
  getContext,
  ThemeStyle,
} from 'msteams-ui-components-react';
import { useRouter } from 'next/router';
import { isInTeams, addTeamsLib, initializeTeams } from './../utils/msteams';
import findPageFontSize from './../utils/find-page-font-size';
import chooseTheme from './../msteams/theme/choose-theme';
import '../styles.css';

addTeamsLib();

function MyApp({ Component, pageProps }) {
  const [fontSize, setFontSize] = useState(16);
  const [theme, setTheme] = useState(ThemeStyle.Light);
  const router = useRouter();
  const { query = {} } = router;

  const updateTheme = useCallback(
    (themeStr) => {
      const chosenTheme = chooseTheme(themeStr);
      setTheme(chosenTheme);
    },
    [setTheme]
  );

  useEffect(() => {
    updateTheme(query.theme);
    const pageFontSize = findPageFontSize();
    setFontSize(pageFontSize);
    initializeTeams();
    microsoftTeams.registerOnThemeChangeHandler(updateTheme);
    // if (isInTeams()) {
    //   initializeTeams();
    //   microsoftTeams.registerOnThemeChangeHandler(updateTheme);
    // }
  }, [query.theme, setFontSize]);

  const context = getContext({ style: theme, baseFontSize: fontSize });

  return (
    <>
      <Head>
        <title>Report Dove | Send work-related reports easily</title>z
      </Head>
      <TeamsThemeContext.Provider value={context}>
        {/* <p>{`query: ${JSON.stringify(query)}`}</p> */}
        <Component {...pageProps} />
      </TeamsThemeContext.Provider>
    </>
  );
}

export default MyApp;

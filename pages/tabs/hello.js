import { useEffect, useCallback } from 'react';
import { UserAgentApplication } from 'msal';

const msalConfig = {
  auth: {
    clientId: '5412575c-2013-4e89-a392-a50286e2b37e',
    authority: 'https://login.microsoftonline.com/common',
    redirectUri: 'https://840c17e3.ngrok.io/auth/tabs/msal-end',
  },
  cache: {
    cacheLocation: 'sessionStorage',
    storeAuthStateInCookie: false,
  },
};

const Hellotab = () => {
  let userAgentApplication;
  const handleSigninSuccess = useCallback((resultKey) => {
    console.log('Login succeeded: ' + resultKey);
    const dataStr = localStorage.getItem(resultKey);
    localStorage.removeItem(resultKey);
    const tokenResult = JSON.parse(dataStr);
    console.log(tokenResult);
  }, []);

  if (typeof window !== 'undefined') {
    userAgentApplication = new UserAgentApplication(msalConfig);
  }

  useEffect(() => {
    console.log('checking for user info:');
    const account = userAgentApplication.getAccount();
    console.log(account);
  }, []);

  const handleSigninFailure = useCallback((reason) => {
    console.log('Login failed: ' + reason);
  }, []);

  const login = useCallback((url) => {
    if (!window.microsoftTeams) return;
    microsoftTeams.authentication.authenticate({
      url: url,
      width: 600,
      height: 535,
      successCallback: handleSigninSuccess,
      failureCallback: handleSigninFailure,
    });
  }, []);

  const loginV1 = useCallback(() => {
    login(`${window.location.origin}/auth/tabs/simple-start`);
  }, []);

  const loginV2 = useCallback(() => {
    login(`${window.location.origin}/auth/tabs/simple-start-v2`);
  }, []);

  const loginSSO = useCallback(async () => {
    console.log('login using SSO');
    try {
      const result = await userAgentApplication.loginPopup({
        scopes: ['User.Read'],
        prompt: 'select_account',
      });
      console.log(result);
    } catch (err) {
      console.log('error');
      console.log(err);
    }
  }, [userAgentApplication]);

  const loginSilent = useCallback(() => {
    console.log('silent login ');
    microsoftTeams.getContext((context) => {
      console.log(context.loginHint);
      userAgentApplication
        .acquireTokenSilent({
          scopes: ['User.Read'],
          loginHint: context.loginHint,
        })
        .then((result) => {
          console.log(result);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }, [userAgentApplication]);

  const logout = useCallback(() => {
    userAgentApplication.logout();
  }, [userAgentApplication]);

  return (
    <main>
      <h1>Welcome to our Tab!</h1>
      <p>
        This sample shows the basics of an authentication/authorization flow in
        a Microsoft Teams tab.
      </p>
      <p>
        Click on the "Login" button below to login to Azure AD, and grant the
        sample app access to your profile information. The app will fetch your
        profile using Microsoft Graph, and display information about you below.
      </p>

      <button onClick={loginSSO}>Login SSO</button>
      <button onClick={loginSilent}>Login Silent</button>
      <button onClick={logout}>Logout</button>
      {/* <button onClick={loginV1}>Login to Azure AD</button>
      <button onClick={loginV2}>Login to Azure AD (V2 endpoint)</button> */}
    </main>
  );
};

export default Hellotab;

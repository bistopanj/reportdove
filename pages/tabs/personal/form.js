import { useCallback } from 'react';
import Form from './../../../components/form';
import withAuth from './../../../hoc/with-auth';
import submitAction from '../../../adapters/submit-action';
import signin from '../../../adapters/auth/login';

const FormPersonalTab = ({
  isAuthenticated,
  accessToken,
  updateAccessToken,
}) => {
  const submitReport = useCallback(
    async (report) => {
      const newAccessToken = await updateAccessToken();

      submitAction({
        job: 'submit-report',
        payload: report,
        accessToken: newAccessToken,
      });
    },
    [accessToken]
  );
  return (
    <Form onReportReady={submitReport} isAuthenticated={isAuthenticated} />
  );
};

export default withAuth(FormPersonalTab);

import Head from 'next/head';

export default function Home() {
  return (
    <div className='container'>
      <Head>
        <title>Create Next App</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main>
        <h1 className='title'>Here we make reporting easier for you</h1>
        <p className='description'>Wait for it!</p>
      </main>
    </div>
  );
}

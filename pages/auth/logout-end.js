import { useEffect } from 'react';
import withAuth from '../../hoc/with-auth';

const LogoutEndPage = ({ signout }) => {
  // useEffect(() => {
  //   signout();
  // }, []);
  return (
    <main className='centeredTextPage'>
      <div>Returning to App...</div>
    </main>
  );
};

export default withAuth(LogoutEndPage);

import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { UserAgentApplication } from 'msal';
import withAuth from '../../hoc/with-auth';
import msalConfig from './../../adapters/auth/msal-config';

const LogoutStartPage = ({ signout }) => {
  const router = useRouter();

  useEffect(() => {
    const {
      query: { tid },
    } = router;
    // if (!tid) return;

    // const authority = `https://login.microsoftonline.com/${tid}`;
    const config = {
      auth: {
        authority: `https://login.microsoftonline.com/${tid}`,
        clientId: msalConfig.auth.clientId,
        postLogoutRedirectUri: msalConfig.auth.postLogoutRedirectUri,
      },
    };

    const userAgent = new UserAgentApplication(config);
    userAgent.logout();
  }, [router]);

  return (
    <main className='centeredTextPage'>
      <div>Logging you out...</div>
    </main>
  );
};

export default withAuth(LogoutStartPage);

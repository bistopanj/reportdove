import { useEffect } from 'react';
import Head from 'next/head';
import { UserAgentApplication } from 'msal';
import getHashParameters from '../../utils/get-hash-parameters';
import { initializeTeams } from '../../utils/msteams';
import msalConfig from './../../adapters/auth/msal-config';

const EndAuthPage = () => {
  useEffect(() => {
    if (!window || !window.microsoftTeams || !window.location) return;
    initializeTeams();
    const hashParams = getHashParameters(window.location.hash);
    if (hashParams['error']) {
      microsoftTeams.authentication.notifyFailure(hashParams['error']);
      // return;
    } else if (hashParams['id_token'] || hashParams['access_token']) {
      const result = {};
      if (hashParams['id_token']) result.idToken = hashParams['id_token'];
      if (hashParams['access_token'])
        result.accessToken = hashParams['access_token'];
      microsoftTeams.authentication.notifySuccess(result);
    } else {
      microsoftTeams.authentication.notifyFailure('UnexpectedFailure');
    }
  }, []);

  return (
    <main className='centeredTextPage'>
      <Head>
        <title>Sign-in Result</title>
      </Head>
      <div>sending you back...</div>
    </main>
  );
};

export default EndAuthPage;

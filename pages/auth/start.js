import { useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { UserAgentApplication } from 'msal';
import msalConfig from './../../adapters/auth/msal-config';
import scopes from './../../adapters/auth/scopes';
import { initializeTeams } from '../../utils/msteams';

const handleRedirect = (error, response) => {
  if (error) {
    console.log('error in login redirect');
    console.log(error);
    alert(`error in redirect: ${error.message}`);
  } else {
    console.log('login successful:');
    const account = userAgent.getAccount();
    console.log(account);
    alert(`welcome: ${JSON.stringify(account)}`);
  }
};

const StartAuthPage = () => {
  const router = useRouter();
  const {
    query: { tid, loginHint, accessToken },
  } = router;

  useEffect(() => {
    initializeTeams();
  }, []);

  useEffect(() => {
    // const authority = `https://login.microsoftonline.com/${tid}`;
    // const tokenRequest = { scopes, loginHint, prompt: 'select_account' };
    const tokenRequest = { scopes, loginHint };
    const userAgent = new UserAgentApplication(msalConfig);
    if (!loginHint && !userAgent.isCallback(window.location.hash)) return;
    userAgent.handleRedirectCallback((error, response) => {
      if (error) {
        console.log(error);
        microsoftTeams.authentication.notifyFailure(error.message);
      } else {
        console.log(response);
        microsoftTeams.authentication.notifySuccess(response);
      }
    });
    const userAccount = userAgent.getAccount();
    console.log(userAccount);
    if (!userAccount || userAgent.isCallback(window.location.hash)) {
      userAgent.loginRedirect(tokenRequest);
    }

    console.log('we are logged in');
    userAgent
      .acquireTokenSilent(tokenRequest)
      .then((result) => {
        console.log(result);
        microsoftTeams.authentication.notifySuccess(result);
      })
      .catch((error) => {
        console.log(error);
        if (
          error.message.indexOf('consent_required') !== -1 ||
          error.message.indexOf('interaction_required') !== -1 ||
          error.message.indexOf('login_required') !== -1
        ) {
          userAgent.acquireTokenRedirect(tokenRequest);
        } else {
          microsoftTeams.authentication.notifyFailure(
            'Error acquiring token: ' + error
          );
        }
      });
  }, [loginHint, handleRedirect, accessToken]);

  return (
    <main className='centeredTextPage'>
      <Head>
        <title>Sign in</title>
      </Head>
      <div>Signing you in...</div>
    </main>
  );
};

export default StartAuthPage;

import { useEffect } from 'react';
import Head from 'next/head';
import getHashParameters from './../../../utils/get-hash-parameters';

const authSimpleEndPage = () => {
  useEffect(() => {
    if (!window || !window.microsoftTeams || !window.location) return;

    localStorage.removeItem('auth.simple.error');
    const hashParams = getHashParameters(window.location.hash);

    if (hashParams['error']) {
      localStorage.setItem('auth.simple.error', JSON.stringify(hashParams));
      microsoftTeams.authentication.notifyFailure(hashParams['error']);
    } else if (hashParams['access_token']) {
      const expectedState = localStorage.getItem('auth.simple.state');
      if (expectedState !== hashParams['state']) {
        localStorage.setItem('auth.simple.error', JSON.stringify(hashParams));
        microsoftTeams.authentication.notifyFailure('StateDoesNotMatch');
      } else {
        let key = 'auth.simple.result';
        localStorage.setItem(
          key,
          JSON.stringify({
            idToken: hashParams['id_token'],
            accessToken: hashParams['access_token'],
            tokenType: hashParams['token_type'],
            expiresIn: hashParams['expires_in'],
          })
        );
        microsoftTeams.authentication.notifySuccess(key);
      }
    } else {
      localStorage.setItem('auth.simple.error', JSON.stringify(hashParams));
      microsoftTeams.authentication.notifyFailure('UnexpectedFailure');
    }
  }, []);

  return (
    <>
      <Head>
        <title>Sign-in result (simple)</title>
      </Head>
      <div>Success...</div>
    </>
  );
};

export default authSimpleEndPage;

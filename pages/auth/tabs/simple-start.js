import { useEffect } from 'react';
import Head from 'next/head';
import { buildAuthProviderEndpoint } from './../../../utils/msteams/build-auth-provider-endpoint';

const authSimpleStartPage = () => {
  useEffect(() => {
    if (!window || !window.microsoftTeams || !window.location) return;

    microsoftTeams.getContext((context) => {
      const authorizationEndpoint = buildAuthProviderEndpoint(context);
      window.location.assign(authorizationEndpoint);
    });
  }, []);

  return (
    <>
      <Head>
        <title>Please sign in (simple)</title>
      </Head>
      <div>Signing you in...</div>
    </>
  );
};

export default authSimpleStartPage;

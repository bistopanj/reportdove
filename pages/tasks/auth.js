import { useEffect } from 'react';
import { useRouter } from 'next/router';
import CenteredContainer from '../../components/centered-container';

const authPage = () => {
  const router = useRouter();
  const { query } = router;
  useEffect(() => {
    if (!microsoftTeams) return;
    const { job, payload: payloadStr, reference, accessToken } = query;
    if (!job) return;
    const payload = JSON.parse(payloadStr);
    const taskData = {
      reference,
      job,
      payload,
    };
    if (accessToken) taskData.accessToken = accessToken;
    microsoftTeams.tasks.submitTask(taskData);
  }, [query]);
  return <CenteredContainer>Sending...</CenteredContainer>;
};

export default authPage;

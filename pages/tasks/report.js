import { useState, useCallback, useEffect } from 'react';
import { useRouter } from 'next/router';
import { PrimaryButton, PanelFooter, Panel } from 'msteams-ui-components-react';
import useSWR from 'swr';
import loadConfig from '../../utils/load-config';
import checkReportAvailability from '../../adapters/check-report-availability';
import styles from './report.module.css';
import withAuth from '../../hoc/with-auth';

const config = loadConfig();

const ReportTaskPage = ({ signin, accessToken }) => {
  console.log('rendering report task page');
  const [ready, setReady] = useState(false);
  const [buttonDisabled, setButtonDisabled] = useState(false);
  const [shouldSave, setShouldSave] = useState(false);
  const router = useRouter();
  const {
    query: { token, display, askForPermission },
  } = router;
  const checkingKey = display ? null : token;
  const { data: isAvailable, error } = useSWR(
    checkingKey,
    checkReportAvailability
  );

  const saveToOnedrive = useCallback(async () => {
    try {
      if (!microsoftTeams) return;
      setButtonDisabled(true);
      signin();
      setShouldSave(true);
      // signin((result) => {
      //   const { accessToken: newAccessToken } = result || {};
      // });
    } catch (error) {
      console.log(error);
      setButtonDisabled(false);
    }
  }, [token]);

  useEffect(() => {
    if (shouldSave && accessToken) {
      microsoftTeams.tasks.submitTask({
        job: 'save-report',
        payload: { token },
        accessToken,
      });
      setShouldSave(false);
      setButtonDisabled(false);
    }
  }, [accessToken, token, shouldSave]);

  // useEffect(() => {
  //   if (askForPermission && accessToken && microsoftTeams) {
  //     console.log('submitting save report job');
  //     microsoftTeams.tasks.submitTask({
  //       job: 'save-report',
  //       payload: { token },
  //       accessToken,
  //     });
  //   }
  // }, [accessToken, token, askForPermission]);

  useEffect(() => {
    if (isAvailable && !display) {
      if (!microsoftTeams) return;
      microsoftTeams.tasks.submitTask({
        job: 'show-report',
        payload: { token, askForPermission },
      });
    }
    if (display) {
      console.log('flagging ready');
      setReady(true);
    }
  }, [isAvailable, token, display, setReady, askForPermission]);

  if (display) {
    const pdfUrl = `${config.BASE_URL}/api/reports?token=${token}`;
    // const iframeSrc = pdfUrl;
    const iframeSrc = `https://docs.google.com/gview?url=${pdfUrl}&embedded=true`;
    return (
      <Panel className={styles.fullPageDialog}>
        {ready && (
          <iframe
            style={{ flex: 1 }}
            src={iframeSrc}
            frameBorder='0'
            allow='autoplay; encrypted-media'
            allowFullScreen=''
          ></iframe>
        )}
        {askForPermission && !accessToken && (
          <PanelFooter className={styles.actionCenter}>
            <p>
              To receive your reports easier, allow us to save them directly to
              your OneDrive.
            </p>
            <PrimaryButton disabled={buttonDisabled} onClick={saveToOnedrive}>
              Save to OneDrive
            </PrimaryButton>
          </PanelFooter>
        )}
        {/* {askForPermission && !accessToken && (
          <PanelFooter className={styles.actionCenter}>
            <p>
              To receive your reports easier, allow us to save them directly to
              your OneDrive.
            </p>
            <PrimaryButton disabled={buttonDisabled} onClick={saveToOnedrive}>
              Save to OneDrive
            </PrimaryButton>
          </PanelFooter>
        )} */}
      </Panel>
    );
  }

  if (!isAvailable)
    return <main className='centeredTextPage'>Creating your report...</main>;

  return <main className='centeredTextPage'>Loading your report...</main>;
};

export default withAuth(ReportTaskPage);

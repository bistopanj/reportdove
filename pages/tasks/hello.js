// import * as microsoftTeams from '@microsoft/teams-js';
import { useEffect } from 'react';
import {
  PrimaryButton,
  TeamsThemeContext,
  ThemeStyle,
  getContext,
} from 'msteams-ui-components-react';
import Head from 'next/head';
import initializeTeams from './../../utils/msteams/initialize-teams';

const params = {
  theme: ThemeStyle.Light,
  fontSize: 16,
};

const Hello = () => {
  const sendSampleCard = () => {
    microsoftTeams.tasks.submitTask({
      email: 'farid.khaheshi@gmail.com',
    });
  };

  const context = getContext({
    baseFontSize: params.fontSize,
    style: params.theme,
  });

  return (
    <>
      <Head>
        <title>Create Next App</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <p className='description'>Hello Zaeem!</p>
      <PrimaryButton onClick={sendSampleCard}>
        Push Me to See the Magic!
      </PrimaryButton>
    </>
  );
};

export default Hello;

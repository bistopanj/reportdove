import permitUpload from './../../../services/images/permit-upload';

const handleGet = (req, res) => {
  const uploadParams = permitUpload();
  res.json(uploadParams);
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'GET':
      return handleGet(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;

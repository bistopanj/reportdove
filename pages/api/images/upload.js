import Busboy from 'busboy';
import saveFile from '../../../services/files/save-file';

const handlePost = async (req, res) => {
  const busboy = new Busboy({ headers: req.headers });
  let buffers = [];
  let fileKey;

  busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
    file.on('data', buffers.push.bind(buffers));
    file.on('end', async () => {
      const fileBuffer = Buffer.concat(buffers);
      fileKey = await saveFile(fileBuffer);
      res.json({ key: fileKey });
    });
  });

  return req.pipe(busboy);
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'POST':
      return handlePost(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export const config = {
  api: {
    bodyParser: false,
  },
};

export default handleReqs;

import adapter from '../../../msteams/adapter';
import MessageExtensionBot from '../../../msteams/bots/message-extension-bot';

const bot = new MessageExtensionBot();

const handleReqs = (req, res) => {
  adapter.processActivity(req, res, async (context) => {
    await bot.run(context);
  });
};

export default handleReqs;

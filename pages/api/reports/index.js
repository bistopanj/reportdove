import { loadFile } from '../../../services/files';

const handleGet = async (req, res) => {
  const {
    query: { token, check },
  } = req;
  if (!token) return res.status(401).send('Not Permitted');
  const file = await loadFile(token);
  if (!file) return res.status(404).send('Not Found');
  if (check) return res.status(200).send('ok');

  res.setHeader('Content-Length', file.length);
  res.setHeader('Content-Type', 'application/pdf');
  res.send(file);
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'GET':
      return handleGet(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;

import fs from 'fs';
import path from 'path';
import { Client } from '@microsoft/microsoft-graph-client';
import { MessageFactory } from 'botbuilder';
import { createReportFileName } from './../../services/reports';
import { findBotConversationReferenceWithUser } from '../../services/sending/find-bot-conversation-reference';
import adapter from './../../msteams/adapter';
import uploadToOnedrive from '../../services/files/upload-to-onedrive';
import createFileAttachment from '../../services/sending/create-file-attachment';

const workingDir = process.cwd();
console.log(workingDir);
const filePath = path.join(workingDir, 'Files', 'transactions.pdf');
const theFile = fs.readFileSync(filePath);
const accessToken =
  'eyJ0eXAiOiJKV1QiLCJub25jZSI6IlEtQk4xTktoNjdBaWxjcXY3Nm1zcWJFd1RCNGVKY0ZfNWV5em05X3MySTQiLCJhbGciOiJSUzI1NiIsIng1dCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSIsImtpZCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSJ9.eyJhdWQiOiJodHRwczovL2dyYXBoLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8wMjg3MWI1Yy1mMTdlLTQ4MDktODA2YS00NzgyM2I4NmE5YzcvIiwiaWF0IjoxNTkwMzc5MDQ4LCJuYmYiOjE1OTAzNzkwNDgsImV4cCI6MTU5MDM4Mjk0OCwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IkFTUUEyLzhQQUFBQWJ1UkEzcVUySEpKTTZlblY4T282dG5hcjlEaVNPVGlURHVYbmIxcEVHWnM9IiwiYW1yIjpbInB3ZCJdLCJhcHBfZGlzcGxheW5hbWUiOiJSZXBvcnQgRG92ZSIsImFwcGlkIjoiNjAzN2Q4M2ItYzUyMy00Njk1LWEwMjUtZTY3ZDJhOTU1N2JlIiwiYXBwaWRhY3IiOiIwIiwiZmFtaWx5X25hbWUiOiJLaGFoZXNoaSIsImdpdmVuX25hbWUiOiJGYXJpZCIsImlwYWRkciI6IjE4NS4xODAuMTUuMjUxIiwibmFtZSI6IkZhcmlkIEtoYWhlc2hpIiwib2lkIjoiNTdkMGI2YzQtNTYwZi00MjUyLThjMmMtMmQwZDI3NjQxYzIyIiwicGxhdGYiOiI1IiwicHVpZCI6IjEwMDMyMDAwQkJDNTU4ODMiLCJzY3AiOiJGaWxlcy5SZWFkV3JpdGUgRmlsZXMuUmVhZFdyaXRlLkFwcEZvbGRlciBvcGVuaWQgcHJvZmlsZSBVc2VyLlJlYWQgZW1haWwiLCJzaWduaW5fc3RhdGUiOlsia21zaSJdLCJzdWIiOiI0ZXB1NEdnczZKTHZXWHdtMHhYQ3lsenRCNl9QMWlMOWNJQ0xxZXFHSEZvIiwidGlkIjoiMDI4NzFiNWMtZjE3ZS00ODA5LTgwNmEtNDc4MjNiODZhOWM3IiwidW5pcXVlX25hbWUiOiJmYXJpZEBiaXN0YXBwcy5vbm1pY3Jvc29mdC5jb20iLCJ1cG4iOiJmYXJpZEBiaXN0YXBwcy5vbm1pY3Jvc29mdC5jb20iLCJ1dGkiOiJqN2lnem9aaG9rZU94UW1LRWNRb0FBIiwidmVyIjoiMS4wIiwieG1zX3N0Ijp7InN1YiI6IlU1c1Y2bWtUck5GamZIOHBiSHJDZDVBM2huSTZYYm1DY0RHbHRaZWh4dGsifSwieG1zX3RjZHQiOjE1ODg0MzAwODl9.N-vQ5KDFscOKH_HyHMFl9MiYUnEO2QYXfBOC6X_pvRzAosmn6Yz0eJPoPlG_QC6M7PqjZ44IKzuLMP0D9GhHR52uxhZnnYt8JAvIbHHeUo2mtUSAKduVvzQYCVkjIpZJAP_Bdq-VBVrDJrF1ZV8xgdmc7bFDMPueOIBOy1L2EAB-aRWrCStCs7Kkw3LuU5DlQ7FVovceK87FTkXMy52EVpfNjxRO1gXheyoIbkGS06NNVgdAicsT8HSIO9MR7tCKUfm4wAf75uUa_cHA5rr41czijKn63HJVrMEFoPZGtvNj--StKz_PWMSmUjF0VLc_c3jZ3Z75i6lcBEoyd1zhOQ';

const authProvider = (cb) => {
  cb(null, accessToken);
};

const options = { authProvider };
const msGraphClient = Client.init(options);

const handleReqs = async (req, res) => {
  try {
    // const { body: accessToken } = req;
    // console.log(accessToken);
    const fileName = createReportFileName();
    // const result = await msGraphClient
    //   .api(`/me/drive/special/approot:/${fileName}/content`)
    // .put(theFile);
    const result = await msGraphClient.api(`/me`).get();
    // const uploadResult = await uploadToOnedrive({
    //   fileBuffer: theFile,
    //   fileName,
    //   accessToken,
    // });
    // const fileAttachment = createFileAttachment({ uploadResult, fileName });
    // const message = MessageFactory.text(
    //   `<b>File uploaded.</b> Your file <b>${fileName}</b> is ready to download`
    // );
    // message.attachments = [fileAttachment];
    // const convRef = await findBotConversationReferenceWithUser(
    //   '57d0b6c4-560f-4252-8c2c-2d0d27641c22'
    // );
    // adapter.continueConversation(convRef, async (context) => {
    //   await context.sendActivity(message);
    //   res.json({ result: uploadResult });
    // });
    res.json({ result });
  } catch (error) {
    console.log(error);
    return res.json({ error });
  }
};

export default handleReqs;

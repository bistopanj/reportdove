require('dotenv').config({
  path: process.env.NODE_ENV === 'production' ? './.env.production' : './.env',
});

module.exports = {
  env: {
    MICROSOFT_APP_ID_DEV: process.env.MICROSOFT_APP_ID_DEV,
    MICROSOFT_APP_SECRET_DEV: process.env.MICROSOFT_APP_SECRET_DEV,
    MICROSOFT_APP_ID: process.env.MICROSOFT_APP_ID,
    MICROSOFT_APP_SECRET: process.env.MICROSOFT_APP_SECRET,
    CLOUDINARY_API_KEY: process.env.CLOUDINARY_API_KEY,
    CLOUDINARY_API_SECRET: process.env.CLOUDINARY_API_SECRET,
    CLOUDINARY_CLOUD_NAME: process.env.CLOUDINARY_CLOUD_NAME,
    REDIS_URI: process.env.REDIS_URI,
    MONGODB_URI: process.env.MONGODB_URI,
    MONGODB_DB_NAME: process.env.MONGODB_DB_NAME,
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(png|svg|jpg|gif|woff|woff2|eot|ttf|otf)$/,
      use: 'file-loader',
    });
    return config;
  },
  // cssLoaderOptions: {
  //   url: false,
  // },
  // cssModules: true,
};

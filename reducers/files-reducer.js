const filesReducer = (state, action) => {
  switch (action.type) {
    case 'add':
      const filesInfo = action.payload.map((file) => ({
        id: `${file.name}-${file.lastModified}-${file.size}-${file.type}`,
        file,
        url: URL.createObjectURL(file),
        description: null,
        uploadStatus: 'LOCAL',
        uploadProgress: 0,
        publicUrl: null,
      }));
      const newMap = filesInfo.reduce(
        (acc, fInfo) => ({ ...acc, [fInfo.id]: fInfo }),
        {}
      );
      const newList = filesInfo.map((f) => f.id);
      return {
        map: { ...state.map, ...newMap },
        list: [...state.list, ...newList],
      };
    case 'update':
      const { id, ...rest } = action.payload;
      return {
        ...state,
        map: { ...state.map, [id]: { ...state.map[id], ...rest } },
      };
    case 'reset':
      return { map: {}, list: [] };
    default:
      return state;
  }
};

export default filesReducer;

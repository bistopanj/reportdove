import { useState, useEffect, useCallback } from 'react';
import { UserAgentApplication } from 'msal';
import getMicrosoftContext from '../utils/msteams/get-microsoft-context';
import msalConfig from './../adapters/auth/msal-config';
import login from './../adapters/auth/login';
import getCachedToken from './../adapters/auth/get-cached-token';

const getDisplayName = (someComponent) =>
  someComponent.displayName || someComponent.name || 'component';

const doNothing = () => ({});

const withAuth = (
  WrappedComponent,
  componentName = `${getDisplayName(WrappedComponent)} With Auth`
) => {
  const WithAuthComponent = (props) => {
    let isAuthenticated = false;
    const [msContext, setMsContext] = useState(null);
    const [userAccount, setUserAccount] = useState(null);
    const [accessToken, setAccessToken] = useState(null);
    const [finished, setFinished] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
      const setUp = async () => {
        try {
          if (!window.userAgent)
            window.userAgent = new UserAgentApplication(msalConfig);
          const microsoftContext = await getMicrosoftContext();
          if (!microsoftContext) return setFinished(true);
          setMsContext(microsoftContext);
          const uAccount = userAgent.getAccount();
          if (uAccount) setUserAccount(uAccount);
          const cachedAccessToken = await getCachedToken(
            userAgent,
            microsoftContext
          );
          if (cachedAccessToken) setAccessToken(cachedAccessToken);
          setFinished(true);
        } catch (err) {
          console.log(`error in auth setup`);
          console.log(err);
          setError(err.message);
        }
      };

      setUp();
    }, []);

    const updateAccessToken = useCallback(async () => {
      const newAccessToken = await getCachedToken(userAgent, msContext);
      setAccessToken(newAccessToken);
      return newAccessToken;
    }, [msContext]);

    const signin = useCallback(async () => {
      try {
        if (!microsoftTeams) {
          return;
        }
        const result = await login(msContext);
        setUserAccount(result.account);
        setAccessToken(result.accessToken);
        return result;
      } catch (err) {
        console.log('error happened when trying to sign in');
        console.log(err);
        alert(err.message);
        setError(err.message);
      }
    }, [msContext]);

    // const getAccessToken = useCallback(async () => {
    //   try {
    //     const theAccount = userAgent.getAccount();
    //     if (!theAccount) {
    //       await login();
    //       const newAccessToken = await getToken(userAgent, msContext);
    //       setAccessToken(newAccessToken);
    //     } else {
    //       const newAccessToken = await getToken(userAgent, msContext);
    //       setAccessToken(newAccessToken);
    //     }
    //   } catch (err) {
    //     console.log('error in getAccessToken');
    //     console.log(err);
    //     alert(err.message);
    //     setError(err.message);
    //   }
    // }, [msContext]);

    const signout = useCallback(() => {
      userAgent.clearCache();
      // console.log('signing user out');
      // const { tid } = msContext;
      // const authority = `https://login.microsoftonline.com/${tid}`;
      // const msalConfigUpdated = {
      //   ...msalConfig,
      //   auth: { ...msalConfig.auth, authority },
      // };
      // const userAgent = new UserAgentApplication(msalConfigUpdated);
      // userAgent.clearCache();
      // updateUserInfo();
    }, []);

    if (error)
      return (
        <main className='centeredTextPage'>
          <div>Something went wrong...</div>
          <div>{error.message || 'error'}</div>
        </main>
      );

    // if (finished && !msContext)
    //   return (
    //     <main className='centeredTextPage'>
    //       <div>Report dove can only be opened inside Microsoft Teams</div>
    //     </main>
    //   );

    isAuthenticated = userAccount ? true : false;

    if (finished) {
      return (
        <WrappedComponent
          msContext={msContext}
          isAuthenticated={isAuthenticated}
          userAccount={userAccount}
          accessToken={accessToken}
          updateAccessToken={updateAccessToken}
          signin={signin}
          signout={signout}
          {...props}
        />
      );
    }
    // return (
    //   <>
    //     <button onClick={signout}>logout</button>
    //       <button onClick={signin}>login</button>
    //     <button onClick={getAccessToken}>getAccessToken</button>
    //     <div>{userAccount?.name}</div>
    //       <div>{userAccount?.userName}</div>
    //       <div>{accessToken ? 'Has Access Token' : 'No Access Token'}</div>
    //     <WrappedComponent
    //       msContext={msContext}
    //       isAuthenticated={isAuthenticated}
    //       userAccount={userAccount}
    //       accessToken={accessToken}
    //       signin={signin}
    //       signout={signout}
    //       {...props}
    //     />
    //   </>
    // );

    return (
      <main className='centeredTextPage'>
        <div>...</div>
      </main>
    );
  };

  WithAuthComponent.displayName = componentName;

  return WithAuthComponent;
};

export default withAuth;

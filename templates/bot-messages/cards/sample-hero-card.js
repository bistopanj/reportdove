import { CardFactory, ActivityTypes, ActionTypes } from 'botbuilder';

const buildSampleHeroCard = () => {
  const message = { type: ActivityTypes.Message };
  const buttons = [
    { type: ActionTypes.ImBack, title: '1. Inline Attachment', value: '1' },
    {
      type: ActionTypes.ImBack,
      title: '2. DownloadFile',
      value: '2',
    },
    {
      type: 'openUrl',
      title: '3. Open Url',
      value:
        'https://reportdove.com/api/reports?token=d6aeeaac-086c-46dd-abfe-82b16ee7c0cd',
    },
  ];
  const cardContent = {
    text: 'You can upload an image or select one of the following choices:',
  };

  const card = CardFactory.heroCard(
    'Hi this is a hero card',
    undefined,
    buttons,
    cardContent
  );
  // console.log(card);
  message.attachments = [card];
  return message;
};

export default buildSampleHeroCard;

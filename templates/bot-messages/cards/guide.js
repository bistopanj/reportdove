import { MessageFactory } from 'botbuilder';

const buildGuideCard = () => {
  const message = MessageFactory.text(`I am a dove! I don't understand human language very well.
  I just do what I am supposed to do.
  I make it easier for you to send your reports. And that's all I can do!
  To send your reports, use the "Report Dove" personal tab. It is very easy.
  Go on! Do it.
  `);

  return message;
};

export default buildGuideCard;

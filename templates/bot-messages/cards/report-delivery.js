import {
  CardFactory,
  ActivityTypes,
  ActionTypes,
  MessageFactory,
} from 'botbuilder';
import loadConfig from '../../../utils/load-config';

const config = loadConfig();

const buildReportDelivaryMessage = (reportKey, fileName) => {
  const url = `${config.BASE_URL}/tasks/report?token=${reportKey}&display=true`;
  const message = MessageFactory.text(
    `<p>Your report is ready.</p>
<a href="${url}">Open It</a>
<br>
<p>If you want to upload the file to your oneDrive, use the following card.</p>
`
  );

  return message;
};

export default buildReportDelivaryMessage;

import loadConfig from '../../utils/load-config';

const config = loadConfig();

const findTask = (context, action) => {
  return {
    task: {
      type: 'continue',
      value: {
        width: 500,
        height: 450,
        title: 'Submit work-related reports easily',
        url: `${config.BASE_URL}/messaging-extensions/form?template=default`,
        fallbackUrl: `${config.BASE_URL}/messaging-extensions/form?template=default`,
      },
    },
  };
};

export default findTask;

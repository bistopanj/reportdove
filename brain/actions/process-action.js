import handleJob from '../jobs';

const processAction = async (context, action) => {
  console.log('action received');
  const { job, payload, accessToken } = action.data;
  console.log(`job: ${job}`);
  const result = await handleJob(job, payload, context, accessToken);
  return result;
};

export default processAction;

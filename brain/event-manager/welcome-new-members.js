const welcomeNewMembers = async (context, next) => {
  console.log('onMembersAdded');

  const membersAdded = context.activity.membersAdded;
  for (const member of membersAdded) {
    if (member.id !== context.activity.recipient.id) {
      await context.sendActivity('Hello and welcome!');
    }
  }
  await next();
};

export default welcomeNewMembers;

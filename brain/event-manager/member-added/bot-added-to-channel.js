import saveConversationReference from '../../memory/save-conversation-reference';

const handleBotAddedToChannel = async (context) => {
  console.log('bot is added to a channel');
  await saveConversationReference(context);
  await context.sendActivity(
    'I saved a reference to this conversation to be able to send messages here.'
  );
};

export default handleBotAddedToChannel;

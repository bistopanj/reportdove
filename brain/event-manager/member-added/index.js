import identifyTypeOfAddingMember from './identify-type-of-adding-member';
import handleBotAddedToChannel from './bot-added-to-channel';
import handleBotAddedToGroupChat from './bot-added-to-group-chat';
import handleBotAddedToPersonalChat from './bot-added-to-personal-chat';
import handleBotAddedToTeam from './bot-added-to-team';
import handleSomeoneAddedToChannel from './someone-added-to-channel';
import handleSomeoneAddedToGroupChat from './someone-added-to-group-chat';

const handlers = {
  'bot-added-to-channel': handleBotAddedToChannel,
  'bot-added-to-group-chat': handleBotAddedToGroupChat,
  'bot-added-to-personal-chat': handleBotAddedToPersonalChat,
  'bot-added-to-team': handleBotAddedToTeam,
  'someone-added-to-channel': handleSomeoneAddedToChannel,
  'someone-added-to-group-chat': handleSomeoneAddedToGroupChat,
};

const handleMemberAdded = async (context, next) => {
  console.log('onMembersAdded');
  const eventType = identifyTypeOfAddingMember(context);
  await context.sendActivity(eventType);

  if (eventType in handlers) {
    await handlers[eventType](context);
  }

  await next();
};

export default handleMemberAdded;

const identifyTypeOfAddingMember = (context) => {
  const botId = context.activity.recipient.id;
  const membersAdded = context.activity.membersAdded;
  const membersAddedIds = membersAdded.map((mem) => mem.id);
  const conversationType = context.activity.conversation.conversationType;

  const whoAdded = membersAddedIds.includes(botId) ? 'bot' : 'someone';

  if (conversationType === 'personal')
    return `${whoAdded}-added-to-personal-chat`;
  else if (conversationType === 'groupChat')
    return `${whoAdded}-added-to-group-chat`;
  else if (conversationType === 'channel') {
    const conversationId = context.activity.conversation.id;
    const teamId = context.activity.channelData.team.id;
    const channelType = teamId === conversationId ? 'team' : 'channel';
    return `${whoAdded}-added-to-${channelType}`;
  }

  return 'unknown';
};

export default identifyTypeOfAddingMember;

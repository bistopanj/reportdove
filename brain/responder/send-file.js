import { MessageFactory } from 'botbuilder';
import { createPdfReport, createReportFileName } from '../../services/reports';
import { saveFile, loadFile } from '../../services/files';

export const sendFileCard = async (context, fileData) => {
  const fileName = createReportFileName();
  const thePDFReport = await createPdfReport(fileData);
  const fileKey = await saveFile(thePDFReport);
  const fileSize = thePDFReport.byteLength;
  const consentContext = { fileName, fileSize, fileKey };
  const fileCard = {
    description: 'This is the file I want to send you',
    sizeInBytes: fileSize,
    acceptContext: consentContext,
    declineContext: consentContext,
  };
  const asAttachment = {
    content: fileCard,
    contentType: 'application/vnd.microsoft.teams.card.file.consent',
    name: fileName,
  };
  await context.sendActivity({ attachments: [asAttachment] });
};

export const uploadAndSendFile = async (context, fileConsentCardResponse) => {
  try {
    const { fileSize, fileKey } = fileConsentCardResponse.context;
    const fileBuffer = await loadFile(fileKey);
    await context.sendActivity('The report will be sent to you in a minute...');
    await fetch(fileConsentCardResponse.uploadInfo.uploadUrl, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/pdf',
        'Content-Length': fileSize,
        'Content-Range': `bytes 0-${fileSize - 1}/${fileSize}`,
      },
      body: fileBuffer,
    });
    const downloadCard = {
      uniqueId: fileConsentCardResponse.uploadInfo.uniqueId,
      fileType: fileConsentCardResponse.uploadInfo.fileType,
    };
    const asAttachment = {
      content: downloadCard,
      contentType: 'application/vnd.microsoft.teams.card.file.info',
      name: fileConsentCardResponse.uploadInfo.name,
      contentUrl: fileConsentCardResponse.uploadInfo.contentUrl,
    };
    const reply = MessageFactory.text(
      `<b>File uploaded.</b> Your file <b>${fileConsentCardResponse.uploadInfo.name}</b> is ready to download`
    );
    reply.textFormat = 'xml';
    reply.attachments = [asAttachment];
    await context.sendActivity(reply);
  } catch (error) {
    console.log(error);
    const reply = MessageFactory.text(
      `<b>Error in uploading the file.</b> Your file <b>${fileConsentCardResponse.uploadInfo.name}</b> was not uploaded`
    );
    reply.textFormat = 'xml';
    await context.sendActivity(reply);
  }
};

export const respondFileConsentDecline = async (
  context,
  fileConsentCardResponse
) => {
  const reply = MessageFactory.text(
    `Declined. We won't upload file <b>${fileConsentCardResponse.context.fileName}</b>.`
  );
  reply.textFormat = 'xml';
  await context.sendActivity(reply);
};

import { TurnContext } from 'botbuilder';
import saveConversationReference from '../memory/save-conversation-reference';
import respondToStaticKeywords from './static-keywords';

const respondMessage = async (context, next) => {
  console.log('onMessage');
  await saveConversationReference(context);
  TurnContext.removeRecipientMention(context.activity);
  const response = respondToStaticKeywords(context.activity.text);
  await context.sendActivity(response);
  await next();
};

export default respondMessage;

import buildSampleHeroCard from '../../../templates/bot-messages/cards/sample-hero-card';
import buildGuideCard from '../../../templates/bot-messages/cards/guide';

const buildResponseToUnknown = () => {
  // const message = buildSampleHeroCard();
  const guideMessage = buildGuideCard();
  // const guideMessage = {
  //   $schema: 'http://adaptivecards.io/schemas/adaptive-card.json',
  //   type: 'AdaptiveCard',
  //   version: '1.0',
  //   body: [
  //     {
  //       type: 'Image',
  //       url: 'https://adaptivecards.io/content/cats/1.png',
  //     },
  //   ],
  // };
  // console.log(message);

  // return guideMessage;
  return guideMessage;
};

export default buildResponseToUnknown;

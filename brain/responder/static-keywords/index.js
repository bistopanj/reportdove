import buildResponseToHelp from './help';
import buildResponseToReports from './reports';
import buildResponseToUnknown from './unknown';

const respondToStaticKeywords = (text) => {
  const lowerCaseText = text.trim().toLocaleLowerCase();
  let response;

  if (lowerCaseText.includes('help')) response = buildResponseToHelp();
  else if (lowerCaseText.includes('reports'))
    response = buildResponseToReports();
  else response = buildResponseToUnknown();

  return response;
};

export default respondToStaticKeywords;

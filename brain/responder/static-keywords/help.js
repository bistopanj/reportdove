import { MessageFactory } from 'botbuilder';

const buildResponseToHelp = () => {
  const message = MessageFactory.text(`
To send your reports, use the "Report Dove" personal tab. It is very easy.
Here, I will send you the reports.
`);
  return message;
};

export default buildResponseToHelp;

import { TurnContext } from 'botbuilder';
import connectToDatabase from './../../dbs/permanent/mongo';

const saveConversationReference = async (context) => {
  console.log('saving conversation reference to use on proactive messaging');
  try {
    const db = await connectToDatabase();
    const convRef = TurnContext.getConversationReference(context.activity);
    const savedRef = await db.collection('conversation-references').updateOne(
      { 'conversation.id': convRef.conversation.id },
      { $set: convRef },
      {
        upsert: true,
      }
    );
  } catch (error) {
    console.log('error happened when trying to save conversation reference');
    console.log(error);
  }
};

export default saveConversationReference;

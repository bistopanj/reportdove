import { TurnContext } from 'botbuilder';

const removeConversationReference = async (context, next) => {
  console.log('onMembersRemoved');
  const membersRemoved = context.activity.membersRemoved;
  for (const member of membersRemoved) {
    if (member.id !== context.activity.recipient.id) {
      await context.sendActivity('Bye bye!');
    }
  }

  await next();
};

export default removeConversationReference;

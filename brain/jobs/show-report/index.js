import loadConfig from '../../../utils/load-config';

const config = loadConfig();

const showReport = async (payload, context) => {
  try {
    const { token, askForPermission } = payload;
    return {
      task: {
        type: 'continue',
        value: {
          width: 'large',
          height: 'large',
          title: 'Your Report Is READY',
          url: `${config.BASE_URL}/tasks/report?token=${token}&display=true${
            askForPermission ? '&askForPermission=true' : ''
          }`,
          fallbackUrl: `${
            config.BASE_URL
          }/tasks/reports?token=${token}&display=true${
            askForPermission ? '&askForPermission=true' : ''
          }`,
        },
      },
    };
  } catch (error) {
    console.log('error in showReport job');
    console.log(error);
  }
};

export default showReport;

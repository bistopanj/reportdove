import submitReport from './submit-report';
import showReport from './show-report';
import saveReport from './save-report';

const jobHandlers = {
  'submit-report': submitReport,
  'show-report': showReport,
  'save-report': saveReport,
};

const handleJob = async (job, payload, context, accessToken) => {
  try {
    if (!job in jobHandlers)
      await context.sendActivity('I cannot handle this process yet! Sorry!');
    const jobResult = await jobHandlers[job](payload, context, accessToken);
    return jobResult;
  } catch (error) {
    console.log('error in job handler');
    console.log(error);
  }
};

export default handleJob;

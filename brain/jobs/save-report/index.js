import { v4 as uuidv4 } from 'uuid';
import { MessageFactory } from 'botbuilder';
import { loadFile } from '../../../services/files';
import findBotConversationReference from '../../../services/sending/find-bot-conversation-reference';
import uploadToOnedrive from '../../../services/files/upload-to-onedrive';
import { createReportFileName } from '../../../services/reports';
import createFileAttachment from '../../../services/sending/create-file-attachment';

const saveReport = async (payload, context, accessToken) => {
  try {
    console.log('saveReport job');
    const fileName = createReportFileName();
    const { token: fileKey } = payload;
    const file = await loadFile(fileKey);
    const convRef = await findBotConversationReference(context);
    if (!accessToken) return;
    const uploadResult = await uploadToOnedrive({
      fileBuffer: file,
      fileName: fileName,
      accessToken,
    });
    const fileAttachment = createFileAttachment({ uploadResult, fileName });
    const reportDelivery = MessageFactory.text(
      `<b>File uploaded.</b> Your file <b>${fileName}</b> is ready to download`
    );
    reportDelivery.attachments = [fileAttachment];
    context.adapter.continueConversation(convRef, async (cntx) => {
      console.log('sending report delivery card after uploading to onedrive');
      await cntx.sendActivity(reportDelivery);
    });
  } catch (error) {
    console.log('error in saveReport');
    console.log(error);
  }
};

export default saveReport;

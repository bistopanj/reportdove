const buildReportFileConsentCard = ({ fileName, fileSize, fileKey }) => {
  const consentContext = { fileName, fileSize, fileKey };
  const fileCard = {
    description: 'Your report is ready.',
    sizeInBytes: fileSize,
    acceptContext: consentContext,
    declineContext: consentContext,
  };
  return {
    content: fileCard,
    contentType: 'application/vnd.microsoft.teams.card.file.consent',
    name: fileName,
  };
};

export default buildReportFileConsentCard;

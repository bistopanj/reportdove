import { v4 as uuidv4 } from 'uuid';
import { MessageFactory } from 'botbuilder';
import createReport from './create-report';
import findBotConversationReference from '../../../services/sending/find-bot-conversation-reference';
import loadConfig from '../../../utils/load-config';
import buildReportDelivaryMessage from '../../../templates/bot-messages/cards/report-delivery';
import uploadToOnedrive from '../../../services/files/upload-to-onedrive';
import createFileAttachment from '../../../services/sending/create-file-attachment';

const config = loadConfig();

const submitReport = async (payload, context, accessToken) => {
  try {
    const reportKey = uuidv4();
    const { file, fileInfo } = await createReport(payload, reportKey);
    console.log('report created');
    const convRef = await findBotConversationReference(context);

    if (accessToken) {
      console.log('we have an accessToken');
      const uploadResult = await uploadToOnedrive({
        fileBuffer: file,
        fileName: fileInfo.fileName,
        accessToken,
      });
      const fileAttachment = createFileAttachment({
        uploadResult,
        fileName: fileInfo.fileName,
      });
      const reportDelivery = MessageFactory.text(
        `<b>File uploaded.</b> Your file <b>${fileInfo.fileName}</b> is ready to download`
      );
      reportDelivery.attachments = [fileAttachment];
      context.adapter.continueConversation(convRef, async (cntx) => {
        console.log('sending report delivery card after uploading to onedrive');
        await cntx.sendActivity(reportDelivery);
      });
    } else {
      console.log('sending report delivery card without uploading to oneDrive');
      const reportDeliveryCard = buildReportDelivaryMessage(reportKey);
      context.adapter.continueConversation(convRef, async (cntx) => {
        await cntx.sendActivity(reportDeliveryCard);
      });
    }
    return {
      task: {
        type: 'continue',
        value: {
          width: 'large',
          height: 'large',
          title: 'Creating Your Report',
          url: `${config.BASE_URL}/tasks/report?token=${reportKey}${
            accessToken ? '' : '&askForPermission=true'
          }`,
          fallbackUrl: `${config.BASE_URL}/tasks/report?token=${reportKey}${
            accessToken ? '' : '&askForPermission=true'
          }`,
        },
      },
    };
  } catch (error) {
    console.log('error in submitReport');
    console.log(error);
  }
};

export default submitReport;

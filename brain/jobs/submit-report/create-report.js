import {
  createReportFileName,
  createPdfReport,
} from '../../../services/reports';
import { saveFile } from '../../../services/files';

const createReport = async (payload, key) => {
  const fileName = createReportFileName();
  const thePDFReport = await createPdfReport(payload);
  const fileKey = await saveFile(thePDFReport, key);
  const fileSize = thePDFReport.byteLength;
  const fileInfo = { fileName, fileSize, fileKey };
  return { file: thePDFReport, fileInfo };
};

export default createReport;

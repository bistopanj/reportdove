import styles from './uploading-image.module.css';

const UploadingImage = ({
  uploadProgress = 0,
  uploadStatus = 'LOCAL',
  ...rest
}) => {
  let blurWidth;
  const blurImage = !(uploadStatus === 'LOCAL' || uploadStatus === 'UPLOADED');
  if (blurImage && uploadStatus !== 'FAILED') {
    blurWidth = `${Math.ceil(100 * (1 - uploadProgress))}%`;
  } else if (blurImage) {
    blurWidth = '100%';
  }
  return (
    <div className={styles.container}>
      {blurImage && (
        <div className={styles.blur} style={{ width: blurWidth }} />
      )}
      <img {...rest} />
    </div>
  );
};

export default UploadingImage;

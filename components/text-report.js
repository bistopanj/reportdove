import { useCallback } from 'react';
import { TextArea } from 'msteams-ui-components-react';
import styles from './text-report.module.css';

const doNothing = () => ({});

const TextReport = ({ report = '', onUpdate = doNothing }) => {
  const handleChange = useCallback(
    (e) => {
      onUpdate(e.target.value);
    },
    [onUpdate]
  );

  return (
    <div className={styles.textReportContainer}>
      <h4>General Report</h4>
      <TextArea
        rows='8'
        label='Details:'
        placeholder='write your report here'
        onChange={handleChange}
        value={report || ''}
        autoFocus
      />
    </div>
  );
};

export default TextReport;

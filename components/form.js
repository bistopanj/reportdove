import { useState, useCallback, useReducer, useEffect } from 'react';
import {
  PrimaryButton,
  PanelFooter,
  Panel,
  PanelBody,
} from 'msteams-ui-components-react';
import FileAdder from './file-adder';
import TextReport from './text-report';
import FilesStack from './files-stack';
import filesReducer from './../reducers/files-reducer';
import uploadImages from './../adapters/upload-images';
import styles from './form.module.css';
import submitAction from '../adapters/submit-action';

const doNothing = () => ({});

const initialFilesState = {
  list: [],
  map: {},
};

const Form = ({ onReportReady = doNothing, isAuthenticated = false }) => {
  const [files, dispatch] = useReducer(filesReducer, initialFilesState);
  const [generalReport, setGeneralReport] = useState('');
  const [waitingToSend, setWaitingToSend] = useState(false);
  const [isProcessing, setIsProcessing] = useState(false);

  const addFiles = useCallback(
    (files) => {
      dispatch({ type: 'add', payload: files });
    },
    [dispatch]
  );

  const updateFileDescription = useCallback(
    (id, description) => {
      dispatch({ type: 'update', payload: { id, description } });
    },
    [dispatch]
  );

  const updateFile = useCallback(
    (id, newData) => {
      dispatch({ type: 'update', payload: { id, ...newData } });
    },
    [dispatch]
  );

  const resetForm = useCallback(() => {
    setWaitingToSend(false);
    setIsProcessing(false);
    setGeneralReport('');
    dispatch({ type: 'reset' });
  }, [setWaitingToSend, setGeneralReport, dispatch]);

  const sendReport = useCallback(async () => {
    const filesList = files.list.map((id) => {
      const theFile = files.map[id];
      return {
        key: theFile.key,
        description: theFile.description,
      };
    });
    const report = { files: filesList, generalReport };
    try {
      resetForm();
      onReportReady(report);
    } catch (error) {
      console.log(error);
    }
  }, [files, generalReport, resetForm]);

  const onFileUploadProgress = useCallback((id, evt) => {
    if (evt.lengthComputable) {
      const percentComplete = evt.loaded / evt.total;
      updateFile(id, {
        uploadProgress: percentComplete,
        uploadStatus: 'UPLOADING',
      });
    }
  }, []);

  const onFileUploadSuccess = useCallback((id, response) => {
    updateFile(id, {
      key: response.key,
      uploadStatus: 'UPLOADED',
    });
  }, []);

  const onFileUploadError = useCallback((id, error) => {
    setIsProcessing(false);
    console.log(`error in uploading image ${id}`);
    console.log(error);
    updateFile(id, { uploadStatus: 'FAILED' });
  }, []);

  const startUploading = useCallback(() => {
    const filesList = files.list.map((id) => files.map[id]);
    setWaitingToSend(true);
    setIsProcessing(true);
    uploadImages(
      filesList,
      onFileUploadSuccess,
      onFileUploadError,
      onFileUploadProgress
    );
  }, [setWaitingToSend, files]);

  useEffect(() => {
    const filesList = files.list.map((id) => files.map[id]);
    if (
      filesList.every((file) => file.uploadStatus === 'UPLOADED') &&
      waitingToSend
    ) {
      sendReport();
    }
  }, [files, waitingToSend, sendReport]);

  return (
    <Panel className={styles.taskDialog}>
      <PanelBody className={styles.taskContent}>
        <TextReport report={generalReport} onUpdate={setGeneralReport} />
        <FilesStack files={files} onUpdate={updateFileDescription} />
        <FileAdder onLoad={addFiles} />
      </PanelBody>
      <PanelFooter className={styles.actionCenter}>
        <PrimaryButton onClick={startUploading} disabled={isProcessing}>
          {isAuthenticated ? 'Send' : 'Send Report'}
        </PrimaryButton>
      </PanelFooter>
    </Panel>
  );
};

export default Form;

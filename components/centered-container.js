import styles from './centered-container.module.css';

const CenteredContainer = ({ children }) => (
  <main className={styles.container}>{children}</main>
);

export default CenteredContainer;

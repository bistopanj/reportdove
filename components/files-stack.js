import FilePreview from './file-preview';

const initialFilesState = {
  list: [],
  map: {},
};

const doNothing = () => ({});

const FilesStack = ({ files = initialFilesState, onUpdate = doNothing }) => {
  const filesList = files.list.map((id) => files.map[id]);
  return (
    <>
      {filesList.map((file) => (
        <FilePreview key={file.id} file={file} onUpdate={onUpdate} />
      ))}
    </>
  );
};

export default FilesStack;

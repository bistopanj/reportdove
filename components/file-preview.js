import { useCallback } from 'react';
import { TextArea } from 'msteams-ui-components-react';
import UploadingImage from './../components/uploading-image';
import styles from './file-preview.module.css';

const doNothing = () => ({});

const FilePreview = ({ file, onUpdate = doNothing }) => {
  const handleChange = useCallback(
    (e) => {
      onUpdate(file.id, e.target.value);
    },
    [onUpdate]
  );

  return (
    <div className={styles.fileReport}>
      <UploadingImage
        src={file.url}
        uploadProgress={file.uploadProgress}
        uploadStatus={file.uploadStatus}
      />
      {/* <p>{`upload status: ${file.uploadStatus}`}</p>
      <p>{`upload progress: ${Math.ceil(file.uploadProgress * 100)}`}</p>
      <p>{`public url: ${file.publicUrl}`}</p> */}
      <div className={styles.reportContainer}>
        <TextArea
          label='More info about this picture:'
          placeholder='write your report here'
          onChange={handleChange}
          value={file.description || ''}
          autoFocus
        />
      </div>
    </div>
  );
};

export default FilePreview;

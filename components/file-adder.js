import { useCallback } from 'react';
import styles from './file-adder.module.css';

const doNothing = () => ({});

const FileAdder = ({ onLoad = doNothing }) => {
  const handleFileAdded = useCallback((e) => {
    onLoad([...e.target.files]);
  }, []);

  return (
    <label className={`${styles.fileInput}`}>
      <input
        type='file'
        accept='image/*'
        name='files'
        multiple
        onChange={handleFileAdded}
      />
      Add Pictures
    </label>
  );
};

export default FileAdder;

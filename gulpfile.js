const gulp = require('gulp');
const del = require('del');
const zip = require('gulp-zip');
const replace = require('gulp-token-replace');
const config = require('./config');
const { version } = require('./package.json');

const cleanOutput = () => {
  console.log('deleting Microsoft Teams App build directory');
  return del(['./msteams/app/build/**/*']);
};

const buildDevelopmentManifest = (done) => {
  const globals = config.development;
  globals.APP_VERSION = version;

  return gulp
    .src(['msteams/app/manifest.json'])
    .pipe(replace({ global: globals }))
    .pipe(gulp.dest('msteams/app/build/development'));
};

const buildProductionManifest = (done) => {
  const globals = config.production;
  globals.APP_VERSION = version;
  return gulp
    .src(['msteams/app/manifest.json'])
    .pipe(replace({ global: globals }))
    .pipe(gulp.dest('msteams/app/build/production'));
};

const buildManifests = gulp.parallel(
  buildProductionManifest,
  buildDevelopmentManifest
);

const copyDevelopmentIcons = (done) => {
  const outlineIcon = `msteams/app/icons/${config.development.OUTLINE_ICON}`;
  const colorIcon = `msteams/app/icons/${config.development.COLOR_ICON}`;
  return gulp
    .src([outlineIcon, colorIcon])
    .pipe(gulp.dest('msteams/app/build/development'));
};

const copyProductionIcons = (done) => {
  const outlineIcon = `msteams/app/icons/${config.production.OUTLINE_ICON}`;
  const colorIcon = `msteams/app/icons/${config.production.COLOR_ICON}`;
  return gulp
    .src([outlineIcon, colorIcon])
    .pipe(gulp.dest('msteams/app/build/production'));
};

const copyIcons = gulp.parallel(copyDevelopmentIcons, copyProductionIcons);

const prepareMsAppResources = gulp.parallel(copyIcons, buildManifests);

const zipDevelopmentApp = (done) => {
  console.log('building MS Teams Development App');
  return gulp
    .src(['msteams/app/build/development/*'])
    .pipe(zip(config.development.ZIP_FILE_NAME))
    .pipe(gulp.dest('msteams/app/build'));
};

const zipProductionApp = (done) => {
  console.log('building MS Teams Production App');
  return gulp
    .src(['msteams/app/build/production/*'])
    .pipe(zip(config.production.ZIP_FILE_NAME))
    .pipe(gulp.dest('msteams/app/build'));
};

const zipApps = gulp.parallel(zipDevelopmentApp, zipProductionApp);

const defaultTask = gulp.series(cleanOutput, prepareMsAppResources, zipApps);

exports.default = defaultTask;

const config = {
  development: {
    ZIP_FILE_NAME: 'reportdove_dev.zip',
    BASE_URL: 'https://8223567e.ngrok.io',
    BOT_ID: '5412575c-2013-4e89-a392-a50286e2b37e',
    PACKAGE_NAME: 'com.reportdove.apps.msteams.dev',
    OUTLINE_ICON: 'reportdove-dev-32x32px.png',
    COLOR_ICON: 'reportdove-dev-color-192x192px.png',
  },
  production: {
    ZIP_FILE_NAME: 'reportdove.zip',
    BASE_URL: 'https://reportdove.com',
    BOT_ID: '6037d83b-c523-4695-a025-e67d2a9557be',
    PACKAGE_NAME: 'com.reportdove.apps.msteams',
    OUTLINE_ICON: 'reportdove-32x32px.png',
    COLOR_ICON: 'reportdove-color-192x192px.png',
  },
  staging: {
    BASE_URL: 'https://beta.reportdove.com',
  },
};

module.exports = config;
